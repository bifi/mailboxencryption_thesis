# Masterarbeit: Untersuchung von Techniken zur persönlichen E-Mail-Postfachverschlüsselung

Als "persönliche E-Mail-Postfachverschlüsselung" wird eine neuartige, 
serverseitige Schutzmaßnahme bezeichnet. Diese verschlüsselt eingehende E-Mails 
und sorgt dafür, dass die verschlüsselten E-Mails ausschließlich durch den 
Nutzer bzw. dessen Passwort entschlüsselt werden können. Solange der 
Nutzer nicht eingeloggt ist, kann der E-Mail-Server keinen Zugriff auf die 
Klartextinhalte der E-Mail erlangen.

Im Rahmen dieser [Masterarbeit](thesis.pdf) werden die folgenden Techniken untersucht:
 * [GPG-Sieve-Filter](https://perot.me/encrypt-specific-incoming-emails-using-dovecot-and-sieve)
 * [MailCrypt](https://wiki.dovecot.org/Plugins/MailCrypt)
 * [Scrambler](https://github.com/posteo/scrambler-plugin)
 * [TREES](https://0xacab.org/riseuplabs/trees)  

Die zugehörige Testumgebung befindet sich in einem weiteren [Repository](https://gitlab.com/bifi/mailencryption_testenvironment).

## Kurzfassung 

E-Mails enthalten häufig sensible und schützenswerte Inhalte, die nicht für 
Dritte bestimmt sind. Obwohl die E-Mail ein weit verbreiteter 
Kommunikationsstandard ist, erfüllt sie viele aktuelle Sicherheitsanforderungen 
nicht.

Eines der Probleme ist, dass E-Mail-Inhalte im Klartext auf den Servern des 
E-Mail-Providers gespeichert sind. Dort haben berechtigte und unberechtigte 
Dritte eine dauerhafte Möglichkeit, auf diese zuzugreifen. Um das zu verhindern,
wurde die sogenannte persönliche E-Mail-Postfachverschlüsselung entwickelt.
Diese neuartige, serverseitige Schutzmaßnahme speichert eingehende E-Mails 
verschlüsselt ab. Sie sorgt dafür, dass die verschlüsselten E-Mails 
ausschließlich durch den Nutzer bzw. dessen Passwort entschlüsselt
werden können.

In dieser [Masterarbeit](thesis.pdf) soll untersucht werden, ob der Einsatz von 
Postfachverschlüsselungstechniken Auswirkungen auf den E-Mail-Server-Betrieb hat
und wie sich diese darstellen. Hierbei werden die vier 
Postfachverschlüsselungstechniken 
[GPG-Sieve-Filter](https://perot.me/encrypt-specific-incoming-emails-using-dovecot-and-sieve), 
[MailCrypt](https://wiki.dovecot.org/Plugins/MailCrypt), 
[Scrambler](https://github.com/posteo/scrambler-plugin) und 
[TREES](https://0xacab.org/riseuplabs/trees) miteinander verglichen. Dazu werden die verschiedenen Techniken 
analysiert, eine Testumgebung entwickelt, sowie eine Messreihe konzipiert 
und durchgeführt. 


## Abstract 


In digital communication, e-mail is an indispensable means of communication.
They often contain sensitive and protective content that is not
intended for third parties. Although e-mail is such an important and widespread
communication standard, it it does not meet modern security requirements.

One problem is that the e-mail content is stored in plain  text on the e-mail 
provider's servers. There, authorized and unauthorized third parties have a
permanent possibility to access them. To prevent this, the so-called personal 
mail box encryption was developed. This new, server-side protective measure
stores incoming e-mails in encrypted form. It ensures that the encrypted 
e-mails can only be decrypted by the user or their password. This means that the
e-mail server can only access the plaintext content as long as the user
is logged in. 
 
The aim of this master thesis is to find out whether the use of mailbox
encryption techniques has an impact on e-mail server operation and how this is 
presented. The four mailbox encryption techniques [GPG-Sieve-Filter](https://perot.me/encrypt-specific-incoming-emails-using-dovecot-and-sieve), 
[MailCrypt](https://wiki.dovecot.org/Plugins/MailCrypt), 
[Scrambler](https://github.com/posteo/scrambler-plugin) and 
[TREES](https://0xacab.org/riseuplabs/trees) will be compared. It is investigated how existing effects 
depend on the mailbox encryption technique used. The techniques will be 
analyzed, a reproducible test environment developed, and a series of 
measurements designed and performed. 